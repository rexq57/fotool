#pragma once
//
//  MachOLoader.h
//  fotool
//
//  Created by Viktor Pih on 2019/6/10.
//  Copyright © 2019 com.wzbbj. All rights reserved.
//

#include <stdint.h>

typedef enum {
    MachOLoaderSourceFile,
    MachOLoaderSourceMemory
}MachOLoaderSource;

typedef struct MachOLoader{
    
    MachOLoaderSource source;
    void* info;
    
    uint32_t magic;
    
    void (*pfread)(void * buffer, long offset, size_t size, void * info);
    void (*pfwrite)(void * buffer, long offset, size_t size, void * info);
    const char* (*pfgets)(char * buffer, long offset, void * info);
    
}MachOLoader;

int MachOLoaderOpenFile(MachOLoader* loader, const char* filename);
int MachOLoaderFromMemory(MachOLoader* loader, void* memory);
void MachOLoaderClose(MachOLoader* loader);
