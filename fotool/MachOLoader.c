//
//  MachOLoader.c
//  fotool
//
//  Created by Viktor Pih on 2019/6/10.
//  Copyright © 2019 com.wzbbj. All rights reserved.
//

#include "MachOLoader.h"
#include <stdio.h>
#include <assert.h>

void fset_read(void * buffer, long offset, size_t size, void * info)
{
    FILE* f = (FILE*)info;
    long old_offset = ftell(f);
    fseek(f, offset & 0xFFFFFFFF, SEEK_SET);
    fread(buffer, size, 1, f);
    fseek(f, old_offset, SEEK_SET);
}

void fset_write(void * buffer, long offset, size_t size, void * info)
{
    assert(offset <= 0xFFFFFFFF);
    
    FILE* f = (FILE*)info;
    long old_offset = ftell(f);
    fseek(f, offset, SEEK_SET);
    fwrite(buffer, size, 1, f);
    fseek(f, old_offset, SEEK_SET);
}

const char* fset_gets(char * buffer, long offset, void * info)
{
    FILE* f = (FILE*)info;
    long old_offset = ftell(f);
    fseek(f, offset & 0xFFFFFFFF, SEEK_SET);
    
    int i=0;
    while( (buffer[i++]=fgetc(f)) != '\0' );
    
    fseek(f, old_offset, SEEK_SET);
    return buffer;
}

int MachOLoaderOpenFile(MachOLoader* loader, const char* filename)
{
    FILE* fp = fopen(filename, "r+");
    
    if (fp == 0)
    {
        printf("无法打开文件!\n");
        return 0;
    }
    printf("file: %p\n", fp);
    
    loader->source = MachOLoaderSourceFile;
    loader->info = fp;
    loader->pfread = fset_read;
    loader->pfwrite = fset_write;
    loader->pfgets = fset_gets;
    
    loader->pfread(&loader->magic, 0, 4, fp);
    
    return 1;
}

void MachOLoaderClose(MachOLoader* loader)
{
    if (loader->info && loader->source == MachOLoaderSourceFile)
    {
        fclose((FILE*)loader->info);
        loader->info = 0;
    }
}

//////////////////////////////////////////////

void mset_read(void * buffer, long offset, size_t size, void * info)
{
    const char* mem = (const char*)info + (offset & 0xFFFFFFFF);
    memcpy(buffer, mem, size);
}

const char* mset_gets(char * buffer, long offset, void * info)
{
    const char* mem = (const char*)info + (offset & 0xFFFFFFFF);
    return mem;//strcpy(buffer, mem);
}

int MachOLoaderFromMemory(MachOLoader* loader, void* memory)
{
    loader->source = MachOLoaderSourceMemory;
    loader->info = memory;
    loader->pfread = mset_read;
    //    loader->pfwrite = fset_write;
    loader->pfgets = mset_gets;
    
    loader->pfread(&loader->magic, 0, 4, memory);
    
    return 1;
}
