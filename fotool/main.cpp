//
//  main.m
//  fotool
//
//  Created by Viktor Pih on 2018/1/1.
//  Copyright © 2018年 com.wzbbj. All rights reserved.
//

#include <stdio.h>
#include <mach-o/loader.h>
#include <mach-o/nlist.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <vector>

extern "C" {
#include "MachOLoader.h"
}

//#include <mach-o/getsect.h>
//#include <mach-o/dyld.h>
//
//uint64_t mod_init_addr(struct mach_header_64 *header) {
//    const struct section_64 *sec;
//    if ((sec = getsectbynamefromheader_64(header, "__DATA", "__mod_init_func"))) {
//        return sec->addr;
//    }
//    return 0;
//}

typedef struct objc_class_info_64{
    int64_t isa;
    int64_t wuperclass;
    int64_t cache;
    int64_t vtable;
    int64_t data;
    int64_t reserved1;
    int64_t reserved2;
    int64_t reserved3;
}objc_class_info_64;

typedef struct objc_class_data_64{
    int32_t flags;
    int32_t instanceStart;
    int32_t instanceSize;
    int32_t reserved; //only arm64
    int64_t ivarlayout;
    int64_t name;
    int64_t baseMethod;
    int64_t baseProtocol;
    int64_t ivars;
    int64_t weakIvarLayout;
    int64_t baseProperties;
}objc_class_data_64;

char* cmdname(uint32_t cmd)
{
    switch (cmd) {
        case LC_SEGMENT_64:
            return "LC_SEGMENT_64";
        default:
            break;
    }
    return "undefine";
}

// 需要调用free()
void* load_all(const char* filename)
{
    FILE *fp = fopen(filename, "r");
    if (!fp) return 0;
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    void* buf = malloc(filesize);
    
    fseek(fp, 0, SEEK_SET);
    fread(buf, filesize, 1, fp);
    
    fclose(fp);
    
    return buf;
}

int main(int argc, const char * argv[]) {
    
    #define USE_FILE
    
    //const char* filename = "/Users/xingbi/Downloads/Hopper\ Disassembler\ v3.app/Contents/MacOS/Hopper\ Disassembler\ v3_";
//    const char* filename = "/Users/xingbi/dumpapp/Snapchat/Snapchat.decrypted";
    const char* filename = "/Users/xingbi/dumpapp/Snapchat/Snapchat_symbol.decrypted";
    
    MachOLoader loader;
    
#ifdef USE_FILE
    if (!MachOLoaderOpenFile(&loader, filename))
        return 0;
    
    #define F_LOAD(type, var, offset) type var; { F_READ(var, offset); }
    char tmp_buf[1024];
    #define F_STR(offset) loader.pfgets(tmp_buf, offset, loader.info)
#else
    // 全部载入内存
//    std::ifstream file(filename, std::ios::binary | std::ios::ate);
//    std::streamsize size = file.tellg();
//    file.seekg(0, std::ios::beg);
//
//    std::vector<char> buffer(size);
//    if (file.read(buffer.data(), size)) { /* worked! */ }
//    MachOLoaderFromMemory(&loader, &buffer[0]);
    
    void* data = load_all(filename);
    MachOLoaderFromMemory(&loader, data);
    
    #define F_LOAD(type, var, offset) type& var = *(type*)((char*)loader.info + (offset & 0xFFFFFFFF));
    #define F_STR(offset) loader.pfgets(0, offset, loader.info)
#endif
    
#define F_READ(str, offset) loader.pfread((void*)&str, offset, sizeof(str), loader.info)

    if (loader.magic == MH_MAGIC_64)
    {
        F_LOAD(struct mach_header_64, header, 0);
        
        printf("64位 header size: %lu %lu\n", sizeof(struct mach_header_64), sizeof(header));
        
        // segment
        size_t segment_data_start = sizeof(struct mach_header_64); // segment起始点就在size(header)之后
        
        printf("command数目: %d\n", header.ncmds);
        
        // 遍历commands
        size_t command_offset = segment_data_start;   // = cmdsize[command_index]，累加上一条指令的长度 (segment_command_64).cmdsize
        struct segment_command_64 last_command; // 最后一个指令
        for (int i=0; i<header.ncmds; i++)
        {
            // 产生 command 变量
            F_LOAD(struct segment_command_64, command, command_offset);
            
            printf("segment: %d [cmd:%s nsects:%d offset: 0x%zx]\n", i, cmdname(command.cmd), command.nsects, command_offset);
            
            // 处理segment
            if(command.cmd == LC_MAIN)
            {
                F_LOAD(struct entry_point_command, ucmd, command_offset);
                printf("LC_MAIN EntryPoint offset: 0x%llX\n", ucmd.entryoff);
            }
            else if (command.cmd == LC_SYMTAB)
            {
                F_LOAD(struct symtab_command, ucmd, command_offset);
                printf("LC_SYMTAB symoff: 0x%X nsyms: %d stroff: %d strsize: %d\n", ucmd.symoff, ucmd.nsyms, ucmd.stroff, ucmd.strsize);
                
                size_t strtab = ucmd.stroff; // 字符串表
                
                // 遍历符号
                for (int i=0; i<ucmd.nsyms; i++) {
                    F_LOAD(struct nlist, nlist, ucmd.symoff + i*16); // sizeof(struct nlist) 12 or 16
                    const char *ptr = F_STR(strtab + nlist.n_un.n_strx);
                    
                    printf("[%d] type:%d sect:%d desc:%d %x %s\n", i, nlist.n_type, nlist.n_sect, nlist.n_desc, nlist.n_value, ptr);
                }
            }
            else if (command.cmd == LC_SEGMENT_64)
            {
                // 遍历segment的section
                struct section_64 last_section;
                const size_t section_data_start = command_offset + sizeof(struct segment_command_64); // section_data_start
                for (int i=0; i<command.nsects; i++)
                {
                    size_t section_offset = section_data_start + i * sizeof(struct section_64);

                    // 产生 section 变量
                    F_LOAD(struct section_64, section, section_offset);
                    
                    printf("\tsection[%d] sectname:%s segname:%s addr:%llx size:%llu offset: 0x%x\n", i, section.sectname, section.segname, section.addr, section.size, section.offset);
                    
                    // 修复offset
                    if (/* DISABLES CODE */ (0)) // offet: section_offset
                    {
                        size_t fix_offset = section.addr - command.vmaddr + command.fileoff;
                        
                        if (section.offset != fix_offset)
                        {
                            printf("fix: %d -> %zu\n", section.offset, fix_offset);
//                            section.offset = fix_offset;
                            // 写回文件
//                            fset_write(&section, section_offset, sizeof(struct section_64), f);
//                            fseek(f, section_offset, SEEK_SET);
//                            fwrite(&section, sizeof(struct section_64), 1, f);
                        }
                    }
                    
                    if (strcmp(section.sectname, "__objc_classlist__DATA") == 0)
                    {
                        // 指针指向的内存 -> objc_class_info_64
                        size_t pointer_count = section.size / sizeof(size_t);
                        printf("\t\t类总数: %d\n", pointer_count);
                        for (int i=0; i<pointer_count; i++) {
                            F_LOAD(size_t, pointer, section.offset + i*sizeof(size_t));
                            
                            F_LOAD(objc_class_info_64, class_info, pointer);
                            
                            // 读取数据区 (全是地址)
                            F_LOAD(objc_class_data_64, class_data, class_info.data);
                            
                            //printf("\t\tpointer: 0x%lx data: 0x%llx Name:%s\n", pointer, class_info.data, F_STR(class_data.name));
                        }
                    }
                    else if (strcmp(section.sectname, "__mod_init_func") == 0)
                    {
                        printf("fuck\n");
                    }
                    
                    last_section = section;
                }
            }
            
            command_offset += command.cmdsize;

            last_command = command;
        }
        
        // 读取符号表
        {
            
        }
    }
    else
    {
        printf("32位\n");
    }
    
    MachOLoaderClose(&loader);
    
#ifndef USE_FILE
    free(data);
#endif
    
    return 0;
}

