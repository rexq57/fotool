//
//  ViewController.m
//  macho_ios
//
//  Created by Viktor Pih on 2019/6/15.
//  Copyright © 2019 com.wzbbj. All rights reserved.
//

#import "ViewController.h"
#include "rebuild_symtab.h"
#include <mach-o/getsect.h>
#include <mach-o/nlist.h>

@interface ViewController ()

@end



@implementation ViewController

- (void) func0
{
    [self func1];
}

- (void) func1
{
    [self func2];
}

- (void) func2
{
    
}

void restore_symbol(const struct mach_header_64* header)
{
    uint32_t command_offset = sizeof(struct mach_header_64);
    struct segment_command_64* last_command;
    uint64_t  entryoff;
    //    printf("sizeof(mach_header_64): %d ncmds_offset: %d %ld\n", sizeof(struct mach_header_64), (uint8_t*)&header->ncmds-(uint8_t*)header, (uint32_t)LC_MAIN);
    uint32_t lc_main = LC_MAIN;
    
#define F_LOAD(type, var, offset) type& var = *(type*)((char*)header + (offset & 0xFFFFFFFF));
    
    for (int i=0; i<header->ncmds; i++)
    {
        F_LOAD(struct segment_command_64, command, command_offset);
        
        //        printf("%d %d %d\n", command_offset, command_offset2-(uint8_t*)header, command.cmdsize);
        
        if (command.cmd == LC_SYMTAB)
        {
            F_LOAD(struct symtab_command, ucmd, command_offset);
            printf("LC_SYMTAB symoff: 0x%X nsyms: %d stroff: %d strsize: %d\n", ucmd.symoff, ucmd.nsyms, ucmd.stroff, ucmd.strsize);
            
            size_t strtab = ucmd.stroff; // 字符串表
            
//            ucmd.symoff = nsymbol.symtab - (uint8_t*)header; // test符号表
//            size_t strtab = nsymbol.strtab - (uint8_t*)header; // test字符串表
            
            // 遍历符号
            for (int i=0; i<ucmd.nsyms; i++) {
                //F_LOAD(struct nlist, nlist, ucmd.symoff + i*16); // sizeof(struct nlist) 12 or 16
                struct nlist& nlist= *(struct nlist*)((uint8_t*)header + ucmd.symoff + i*16);
                const char *ptr = (const char*)((uint8_t*)header + strtab + nlist.n_un.n_strx);
                
                uint8_t n_sect = sect_for_address(nlist.n_value + 0x100000000, header);
                uint8_t n_type = nlist.n_value == 0 ? 1 : N_PEXT | N_SECT;
                
                printf("%llx [%d] %d %d type:%d sect:%d desc:%d %x %s\n", ucmd.symoff + i*16, i, n_sect, n_type, nlist.n_type, nlist.n_sect, nlist.n_desc, nlist.n_value, ptr);
                //                printf("[%d] type:%d sect:%d desc:%d value:%x %s\n", i, nlist.n_type, nlist.n_sect, nlist.n_desc, nlist.n_value, ptr);
            }
        }
        
        command_offset += command.cmdsize;
    }
}

//static symbol_addr_map symbolList;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    const char* filename = "/Library/MobileSubstrate/DynamicLibraries/snapchat_symbol.txt";
    
//    restore_symbol((const struct mach_header_64*)_dyld_get_image_header(0));
    
//    if (!read_symbol(filename, &symbolList))
//    {
//        printf("error\n");
//        return;
//    }
    
    
}


@end
