//
//  AppDelegate.h
//  macho_ios
//
//  Created by Viktor Pih on 2019/6/15.
//  Copyright © 2019 com.wzbbj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

