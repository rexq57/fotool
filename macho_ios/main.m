//
//  main.m
//  macho_ios
//
//  Created by Viktor Pih on 2019/6/15.
//  Copyright © 2019 com.wzbbj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
