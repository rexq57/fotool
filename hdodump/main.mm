//
//  main.c
//  hdodump
//
//  Created by Viktor Pih on 2018/1/1.
//  Copyright © 2018年 com.wzbbj. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include "pthread.h"

#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/sysctl.h>

#include <mach/mach.h>
#include <mach/mach_init.h>
#include <mach/mach_vm.h>

#include <mach-o/loader.h>

mach_vm_address_t getBasicAddress(mach_port_t task){
    mach_vm_size_t region_size = 0;
    mach_vm_address_t region = NULL;
    int ret = 0;
    
    /* Get region boundaries */
#if defined(_MAC64) || defined(__LP64__)
    vm_region_basic_info_data_64_t info;
    mach_msg_type_number_t info_count = VM_REGION_BASIC_INFO_COUNT_64;
    vm_region_flavor_t flavor = VM_REGION_BASIC_INFO_64;
    if ((ret = mach_vm_region(mach_task_self(), &region, &region_size, flavor, (vm_region_info_t)&info,
                              (mach_msg_type_number_t*)&info_count, (mach_port_t*)&task)) != KERN_SUCCESS)
    {
        printf("mach_vm_region() message %s!\n",mach_error_string(ret));
        return NULL;
    }
#else
    vm_region_basic_info_data_t info;
    mach_msg_type_number_t info_count = VM_REGION_BASIC_INFO_COUNT;
    vm_region_flavor_t flavor = VM_REGION_BASIC_INFO;
    if ((ret = vm_region(mach_task_self(), &region, &region_size, flavor, (vm_region_info_t)&info,
                         (mach_msg_type_number_t*)&info_count, (mach_port_t*)&task)) != KERN_SUCCESS)
    {
        printf("vm_region() message %s!\n",mach_error_string(ret));
        return NULL;
    }
#endif
    return region;
}
vm_size_t readRemotoMemory(char *buf,vm_size_t len,mach_port_t task,vm_address_t address)
{
    vm_size_t outSize = 0;
    
    int ret = vm_read_overwrite(task,address,len,(vm_address_t)buf,&outSize);
    if (ret != 0)
    {
        printf("vm_read_overwrite() message %s!\n",mach_error_string(ret));
        return 0;
    }
    return outSize;
}

int fixOffset(FILE* f, mach_port_t task, mach_vm_address_t baseAddress)
{
    if (f == 0)
    {
        printf("无法打开文件!\n");
        return -1;
    }
    printf("file: %d\n", f);
    
    char buffer[512];
    
    // 读取mach_header
    // 先判断架构：通过文件头4字节判断
    uint32_t magic;
    fseek(f, 0, SEEK_SET);
    fread(&magic, 4, 1, f);
    if (magic == MH_MAGIC_64)
    {
        printf("64位\n");
        
        size_t copyStart = 0;
        size_t copyEnd = 0;
        
        mach_header_64 header;
        fseek(f, 0, SEEK_SET);
        fread(&header, sizeof(mach_header_64), 1, f);
        
        // segment
        size_t segment_start = sizeof(mach_header_64);
        
        printf("command数目: %d\n", header.ncmds);
        segment_command_64 last_command;
        size_t command_start = segment_start;
        for (int i=0; i<header.ncmds; i++)
        {
            segment_command_64 command;
            {
                // 第一个command可以直接读
                // 其余的根据上一个command的长度来计算
                if (i > 0)
                {
                    command_start += last_command.cmdsize;
                }
            }
            
            fseek(f, command_start, SEEK_SET);
            fread(&command, sizeof(segment_command_64), 1, f);
            
            printf("segment: %d [%x]\n", i, command_start);
            
            bool isFirstCommand = i == 1;
            bool isWritted = false;
            
            // 遍历command下面的section
            if (command.cmd == LC_SEGMENT_64)
            {
                section_64 last_section;
                for (int i=0; i<command.nsects; i++)
                {
                    section_64 section;
                    size_t section_start = command_start + sizeof(segment_command_64);
                    {
                        if (i > 0)
                        {
                            section_start += i * sizeof(section_64);
                        }
                    }
                    
                    fseek(f, section_start, SEEK_SET);
                    fread(&section, sizeof(section_64), 1, f);
                    
                    // 修复offset
                    if (true)
                    {
                        size_t fix_offset = section.addr - command.vmaddr + command.fileoff;
                        
                        if (section.offset != fix_offset)
                        {
                            printf("fix: %d -> %d\n", section.offset, fix_offset);
                            section.offset = fix_offset;
                            // 写回文件
                            fseek(f, section_start, SEEK_SET);
                            fwrite(&section, sizeof(section_64), 1, f);
                        }
                    }
                    
                    // 30AA00
                    // 36CBD0
                    
                    // 复制解码后的数据
//                    static int fuck_i=0;
                    
                    // 统计 __objc_methname 起始点
                    // __objc_methtype 终止点
                    
                    if (isFirstCommand)
                    {
                        if (strcmp(section.sectname, "__objc_methname") == 0)
                            copyStart = section.addr - command.vmaddr;
                        if (strcmp(section.sectname, "__objc_methtype") == 0)
                            copyEnd = section.addr - command.vmaddr + section.size;
                        
                        // 必须以数据段来拷贝，单个拷贝中间不连续，会发生崩溃，不知道什么原因
                        //        if (
                        //            isFirstCommand && copyEnd != 0
                        //            //                        (
                        //            //                        strcmp(section.sectname, "__objc_methname") == 0
                        //            //                        || strcmp(section.sectname, "__cstring") == 0
                        //            //                        || strcmp(section.sectname, "__gcc_except_tab") == 0
                        //            //                        || strcmp(section.sectname, "__const") == 0
                        //            //                        || strcmp(section.sectname, "__ustring") == 0
                        //            //                        || strcmp(section.sectname, "__objc_classname") == 0
                        //            //                        || strcmp(section.sectname, "__objc_methtype") == 0
                        //            //                        )
                        //            )
                        //            //                    if (false && strcmp(section.segname, "__TEXT") == 0)
                        if (!isWritted && copyEnd != 0)
                        {
                            //                        size_t n = section.size; // 字节数
                            //                        size_t n = 0x36CBD0 - 0x30AA00;
                            size_t n = copyEnd - copyStart;
                            size_t toread, r;
                            //            vm_address_t offset = section.addr - command.vmaddr; // 偏移地址
                            //                        vm_address_t offset = 0x30AA00;
                            vm_address_t offset = copyStart;
                            vm_address_t address = baseAddress + offset;
                            //            printf("section.addr: %llx command.vmaddr: %llx\n", section.addr, command.vmaddr);
                            printf("复制section数据: 0x%llx 0x%llx 0x%llx (0x%llx)\n", baseAddress, offset, n, offset+n);
                            
                            fseek(f, offset, SEEK_SET);
                            while (n > 0) {
                                toread = (n > sizeof(buffer)) ? sizeof(buffer) : n;
                                
                                r = readRemotoMemory(buffer, toread, task, address);
                                
                                {
                                    address += r;
                                    n -= r;
                                    r = fwrite(buffer, 1, r, f);
                                    
                                    if (r != toread) {
                                        printf("不可能\n");
                                        break;
                                    }
                                }
                            }
                            
                            isWritted = true;
                        }
                    }

                    last_section = section;
                }
            }
            
            last_command = command;
        }
        

    }
    else
    {
        printf("32位\n");
    }
    
    return 1;
}



#if 1

// 实际起作用的处理函数
// 制作一个dump副本，修正了offset，并从当前已加载程序内存里拷贝解密后的数据段
void* handler2(void *p)
{
    //int pid = 16057;
    int pid = getpid();
    char buffer[512];
    mach_vm_address_t address = 0;
    mach_port_t task = 0;
    
    int waitTime = 15;
    while(waitTime){
        sleep(1);
        printf("thread waiting! %d\n",waitTime);
        waitTime --;
    }
    
    int ret = task_for_pid(mach_task_self(),pid,&task);
    if (ret != 0)
    {
        printf("task_for_pid() message %s!\n",mach_error_string(ret));
        return NULL;
    }
    address = getBasicAddress(task);
    
    printf("pid     : %d\n",pid);
    printf("task    : %x\n", task);
    printf("address : %llx\n", address);
    
    if (address == 0)
    {
        printf("getBasicAddress() faild!\n");
        return NULL;
    }
    
    uint32_t writeSize = 0;
    
    // 打开源文件
    FILE *fp_src = fopen((char*)p,"r");
    FILE *fp_dst = fopen("dump","w+");
    if (fp_src != NULL || fp_dst != NULL)
    {
        // 复制全部数据
        {
            fseek(fp_src, 0L, SEEK_END);
            size_t n = ftell(fp_src);
            fseek(fp_src, 0L, SEEK_SET);
            char buffer[512];
            size_t r, toread;

            while (n > 0) {
                toread = (n > sizeof(buffer)) ? sizeof(buffer) : n;
                r = fread(buffer, 1, toread, fp_src);
                
                {
                    n -= r;
                    r = fwrite(buffer, 1, r, fp_dst);
                    
                    if (r != toread) {
                        break;
                    }
                }
            }
        }
        
        // 修复offset
        fixOffset(fp_dst, task, address);
        
        printf("path: %s\n", (char*)p);
    }
    
    
    fclose(fp_dst);
    fclose(fp_src);
    
    return NULL;
}

//int main(int argc, char const *argv[])
void* handler(void *p)
{
    //int pid = 16057;
    int pid = getpid();
    char buffer[512];
    mach_vm_address_t address = 0;
    mach_port_t task = 0;
    
    int waitTime = 3;
    while(waitTime){
        sleep(1);
        printf("thread waiting! %d\n",waitTime);
        waitTime --;
    }
    
    int ret = task_for_pid(mach_task_self(),pid,&task);
    if (ret != 0)
    {
        printf("task_for_pid() message %s!\n",mach_error_string(ret));
        return NULL;
    }
    address = getBasicAddress(task);
    
    printf("pid     : %d\n",pid);
    printf("task    : %x\n", task);
    printf("address : %llx\n", address);
    
    if (address == 0)
    {
        printf("getBasicAddress() faild!\n");
        return NULL;
    }
    
    uint32_t writeSize = 0;
    FILE *fp = fopen("dump.bin","wb");
    
    readRemotoMemory(buffer,512,task,address);
    printf("%x\n",*(uint*)buffer);
    
    // 50MB 上限
    while (writeSize <= 0x5000000){
        
        size_t size = readRemotoMemory(buffer,512,task,address);
        
        if (size == 0)
            break;
        printf("%x\n",*(uint*)buffer);
        address += size;
        writeSize += size;
        fwrite(buffer,size,1,fp);
    }
    
    fclose(fp);
    
    return NULL;
}
#else
// 这个帖子作者写的demo，不能导出后面的程序体 0x1F8E 要改大才行
//int main(int argc, char const *argv[])
void* handler(void *p)
{
    //int pid = 16057;
    int pid = getpid();
    char buffer[512];
    mach_vm_address_t address = 0;
    mach_port_t task = 0;
    
    int waitTime = 15;
    while(waitTime){
        sleep(1);
        printf("thread waiting! %d\n",waitTime);
        waitTime --;
    }
    
    int ret = task_for_pid(mach_task_self(),pid,&task);
    if (ret != 0)
    {
        printf("task_for_pid() message %s!\n",mach_error_string(ret));
        return NULL;
    }
    address = getBasicAddress(task);
    
    printf("pid     : %d\n",pid);
    printf("task    : %x\n", task);
    printf("address : %llx\n", address);
    
    if (address == 0)
    {
        printf("getBasicAddress() faild!\n");
        return NULL;
    }
    
    uint32_t writeSize = 0;
    FILE *fp = fopen("dump","wb");
    readRemotoMemory(buffer,512,task,address);
    printf("%x\n",*(uint*)buffer);
    
    while (writeSize <= 0x1F8E){
        readRemotoMemory(buffer,512,task,address);
        //printf("%x\n",*(uint*)buffer);
        address += 512;
        writeSize += 512;
        fwrite(buffer,512,1,fp);
    }
    fclose(fp);
    
    return NULL;
}
#endif

__attribute__((constructor))
void init(int argc, const char **argv, const char **envp, const char **apple, struct ProgramVars *pvars)
{
    int err;
    pthread_t ntid;
    err = pthread_create(&ntid, NULL, handler2, (void*)argv[0]);
    if (err != 0)
    {
        printf("can't create thread: %s\n", strerror(err));
        return ;
    }
}
