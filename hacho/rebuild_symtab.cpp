//
//  rebuild_symtab.cpp
//  macho
//
//  Created by Viktor Pih on 2019/6/15.
//  Copyright © 2019 com.wzbbj. All rights reserved.
//

#include "rebuild_symtab.h"
#include <mach-o/getsect.h>
#include <mach-o/nlist.h>
#include <string>
#include <vector>
#include <functional>

#define SET_FIND(s, v) ((s).find(v) != (s).end())
#define MAP_FIND SET_FIND

// 需要调用free()
void* load_all(const char* filename)
{
    FILE *fp = fopen(filename, "r");
    if (!fp) return 0;
    
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    void* buf = malloc(filesize);
    
    fseek(fp, 0, SEEK_SET);
    fread(buf, filesize, 1, fp);
    
    fclose(fp);
    
    return buf;
}

uint64_t entryoff(const struct mach_header_64* header)
{
    uint8_t* segment_data_start = (uint8_t*)header + sizeof(struct mach_header_64);
    uint8_t* command_offset = segment_data_start;
    struct segment_command_64* last_command;
    uint64_t  entryoff;
    //printf("sizeof(mach_header_64): %d ncmds_offset: %d %ld\n", sizeof(struct mach_header_64), (uint8_t*)&header->ncmds-(uint8_t*)header, (uint32_t)LC_MAIN);
    uint32_t lc_main = LC_MAIN;
    for (int i=0; i<header->ncmds; i++)
    {
        struct segment_command_64* command = (struct segment_command_64*)command_offset;
        
        if(command->cmd == LC_MAIN)
        {
            struct entry_point_command* ucmd = (struct entry_point_command*)command_offset;
            entryoff = ucmd->entryoff;
            break;
        }
        
        command_offset += command->cmdsize;
        last_command = command;
    }
    return (uint64_t)entryoff;
}

void read_symbol_func(const char* addr, const char* symbol, void* info)
{
    symbol_addr_map* list = (symbol_addr_map*)info;
    size_t naddr = strtol(addr, NULL, 16);
    
    if (MAP_FIND(*list, naddr))
    {
        // 如果后来者重复的是sub_开头的符号，则跳过覆盖
        if (*(int*)symbol == 'sub_')
        {
            return;
        }
        //                else
        //                {
        //                    printf("支持覆盖 %s %s\n", (*list)[naddr].c_str(), symbol);
        //                }
    }
//    list->insert(std::pair<size_t, std::string>(naddr, symbol));
    (*list)[naddr] = symbol;
};

int read_symbol(const char* filename, void* info, pfunc_read func)
{
//    const char* filename = "/Library/MobileSubstrate/DynamicLibraries/snapchat_symbol.txt";
    const char *delim = " t ";
    size_t delimSize = (size_t)strlen(delim);
    
    int bufsize = 1024 * 10; // 10KB
    char* str = (char*)malloc(bufsize);
    
    FILE *_fp = (FILE*)fopen(filename, "r");
    if (!_fp) return 0;
    
    while(!(int)feof(_fp))
    {
        (void)fgets(str, bufsize-1, _fp); // 包括换行符
        char* endpos = &str[(size_t)strlen(str)-1];
        if (*endpos == '\n') *endpos = '\0';
        
        char* pos = (char*)strstr(str, delim);
        if (!pos) continue;
        
        *pos = '\0';
        
        const char* straddr = str;
        const char* strsymbol = pos + delimSize;
        
        (void)func(straddr, strsymbol, info);
    }
    (void)fclose(_fp);
    
    (void)free(str);
    
    return 1;
}

uint8_t sect_for_address(uint64_t address, const struct mach_header_64* header)
{
    uint32_t command_offset = sizeof(struct mach_header_64);
    struct segment_command_64* last_command;
    uint32_t lc_main = LC_MAIN;
    uint8_t n_sect = 0;
#define F_LOAD(type, var, offset) type& var = *(type*)((char*)header + (offset & 0xFFFFFFFF));
    
    for (int i=0; i<header->ncmds; i++)
    {
        F_LOAD(struct segment_command_64, segmentCommand, command_offset);
        
        if (segmentCommand.cmd == LC_SEGMENT_64)
        {
            //            printf("检查segment %llx %llx %llu\n", address, segmentCommand.vmaddr, segmentCommand.vmsize);
            
            if (!(address >= segmentCommand.vmaddr) && (address < segmentCommand.vmaddr + segmentCommand.vmsize))
            {
                //                printf("不包含 %d %d %d\n", address, segmentCommand.vmaddr, segmentCommand.vmsize);
                n_sect += segmentCommand.nsects;
            }
            else
            {
                // 遍历segment的section
                const size_t section_data_start = command_offset + sizeof(struct segment_command_64); // section_data_start
                for (int i=0; i<segmentCommand.nsects; i++)
                {
                    size_t section_offset = section_data_start + i * sizeof(struct section_64);
                    F_LOAD(struct section_64, section, section_offset);
                    n_sect ++;
                    
                    //                    printf("检查section %llx %llx %llx\n", address, section.addr, section.size);
                    if ((address >= section.addr) && (address < section.addr + section.size))
                    {
                        return n_sect;
                    }
                }
            }
        }
        
        command_offset += segmentCommand.cmdsize;
    }
    
    return 0;
}

const struct symtab_command* get_symtab(const struct mach_header_64* header)
{
    uint32_t command_offset = sizeof(struct mach_header_64);
    struct segment_command_64* last_command;
    uint32_t lc_main = LC_MAIN;
    
#define F_LOAD(type, var, offset) type* var = (type*)((char*)header + (offset & 0xFFFFFFFF));
    
    for (int i=0; i<header->ncmds; i++)
    {
        F_LOAD(struct segment_command_64, command, command_offset);
        
        if (command->cmd == LC_SYMTAB)
        {
            F_LOAD(struct symtab_command, ucmd, command_offset);
            //            printf("LC_SYMTAB symoff: 0x%X nsyms: %d stroff: %d strsize: %d\n", ucmd.symoff, ucmd.nsyms, ucmd.stroff, ucmd.strsize);
            return ucmd;
        }
        
        command_offset += command->cmdsize;
    }
    printf("没有找到符号表!\n");
    exit(0);
    return 0;
}

#ifdef __LP64__
typedef struct mach_header_64 mach_header_t;
typedef struct segment_command_64 segment_command_t;
typedef struct section_64 section_t;
typedef struct nlist_64 nlist_t;
#define LC_SEGMENT_ARCH_DEPENDENT LC_SEGMENT_64
#else
typedef struct mach_header mach_header_t;
typedef struct segment_command segment_command_t;
typedef struct section section_t;
typedef struct nlist nlist_t;
#define LC_SEGMENT_ARCH_DEPENDENT LC_SEGMENT
#endif

//typedef void (*pfunc_read_symtab)(const struct nlist* nlist, void* info);
void read_symtab(const struct mach_header_64* header, pfunc_read_symtab func, void* info)
{
    uint32_t command_offset = sizeof(struct mach_header_64);
    struct segment_command_64* last_command;
    uint32_t lc_main = LC_MAIN;
    
    #define F_LOAD(type, var, offset) type& var = *(type*)((char*)header + (offset & 0xFFFFFFFF));
    
    for (int i=0; i<header->ncmds; i++)
    {
        F_LOAD(struct segment_command_64, command, command_offset);
        
        if (command.cmd == LC_SYMTAB)
        {
            F_LOAD(struct symtab_command, symtab_cmd, command_offset);
            //printf("LC_SYMTAB symoff: 0x%X nsyms: %d stroff: %d strsize: %d\n", ucmd.symoff, ucmd.nsyms, ucmd.stroff, ucmd.strsize);
            size_t strtab = symtab_cmd.stroff; // 字符串表
            
            // 遍历符号表
            for (int i=0; i<symtab_cmd.nsyms; i++) {
                //F_LOAD(struct nlist, nlist, ucmd.symoff + i*16); // sizeof(struct nlist) 12 or 16
                struct nlist& nlist= *(struct nlist*)((uint8_t*)header + symtab_cmd.symoff + i*16);
                const char *ptr = (const char*)((uint8_t*)header + strtab + nlist.n_un.n_strx);
                
                func(&nlist, ptr, info);
            }
        } else if (command.cmd == LC_DYSYMTAB) {
//            F_LOAD(struct dysymtab_command, dysymtab_cmd, command_offset);
//            F_LOAD(struct symtab_command, symtab_cmd, command_offset);
//
//            nlist_t* symtab = (nlist_t*)((uintptr_t)header + symtab_cmd.symoff);
//            printf("%d\n", sizeof(symtab));
//
////            //printf("LC_SYMTAB symoff: 0x%X nsyms: %d stroff: %d strsize: %d\n", ucmd.symoff, ucmd.nsyms, ucmd.stroff, ucmd.strsize);
//            char* strtab = (char*)((uint8_t*)header + symtab_cmd.stroff); // 字符串表
//
//            // 动态符号表地址 = 基址 + 动态符号表偏移量
//            uint32_t *indirect_symtab = (uint32_t *)((uintptr_t)header + dysymtab_cmd.indirectsymoff);
//
//            uint32_t *indirect_symbol_indices = indirect_symtab;// + section->reserved1;
//
//            // 遍历符号表
//            for (int i=0; i<dysymtab_cmd.nindirectsyms; i++) {
//                uint32_t symtab_index = indirect_symbol_indices[i];
////                uint32_t symtab_index = i;//indirect_symtab[i];
//                uint32_t strtab_offset = symtab[symtab_index].n_un.n_strx;
//                char *symbol_name = strtab + strtab_offset;
////                //F_LOAD(struct nlist, nlist, ucmd.symoff + i*16); // sizeof(struct nlist) 12 or 16
////                struct nlist& nlist= *(struct nlist*)((uint8_t*)header + ucmd.indirectsymoff + i*16);
////                const char *ptr = (const char*)((uint8_t*)header + strtab + nlist.n_un.n_strx);
////
////                func(&nlist, ptr, info);
//                printf("[%d] %p %s\n", i, strtab_offset, symbol_name);
//            }
        }
        
        command_offset += command.cmdsize;
    }
}

bool create_symbol_with_list(const symbol_addr_map& symbolList_, const struct mach_header_64* header, new_symbol* result)
{
    const struct symtab_command symtab = *get_symtab(header);
    size_t strtab = symtab.stroff; // 字符串表
    
    // 制作副本symbolList用于剔除重复于现有符号表里的符号
    symbol_addr_map symbolList = symbolList_;
    {
        // 去除重复
        struct read_symtab_info {
            const struct mach_header_64* header;
            size_t strtab;
            symbol_addr_map* symbolList;
        };
        
        read_symtab_info info = {header, strtab, &symbolList};
        read_symtab(header, [](const struct nlist* nlist, const char* symptr, void* info){
            read_symtab_info* pinfo = (read_symtab_info*)info;
            //const char *ptr = (const char*)((uint8_t*)pinfo->header + pinfo->strtab + nlist->n_un.n_strx);
            uint32_t key = nlist->n_value;
            
            // 检查symbolList里是否重复，将重复的删除
            auto it = pinfo->symbolList->find(key);
            if (it != pinfo->symbolList->end())
                pinfo->symbolList->erase(key);
            
        }, &info);
    }
    
    // 计算需要添加的符号表长度、字符串长度
    const size_t symsize = 16;
    size_t symtabsize_ex = symbolList.size()*symsize;
    size_t strsize_ex = 0;
    {
        for (auto it : symbolList) {
            strsize_ex += it.second.length() + 1; // 算上'\0'
        }
    }
    
    //    printf("total %d ex %d length %d\n", symbolList_.size(), symbolList.size(), strsize_ex);
    
    // 需要转移符号表，先读取已经有的，写入申请的内存
    // 需要申请，符号表内存+字符串表内存
    
    size_t old_symtabsize = symtab.nsyms*symsize;
    size_t old_strsize = symtab.strsize;
    size_t new_symsize = old_symtabsize + symtabsize_ex;
    size_t new_strsize = old_strsize + strsize_ex;
    //std::shared_ptr<uint8_t> _newTabData(new uint8_t[new_symsize+new_strsize]);
    //uint8_t* new_tabdata = _newTabData.get();
    uint8_t* memptr = (uint8_t*)malloc(new_symsize+new_strsize);
    uint8_t* new_symtab = memptr;
    uint8_t* new_strtab = (uint8_t*)new_symtab + new_symsize;
    
    //    printf("old 符号表 offset %llx-%llx  字符串表长度 %llx-%llx\n", symtab.symoff, symtab.symoff + old_symtabsize, symtab.stroff + new_strsize, symtab.strsize);
    //    printf("new 符号表 mem %llx-%llx  字符串表长度 %llx-%llx\n", new_symtab, new_symtab + new_symtabsize, new_strtab, new_strtab+new_strsize);
    
    // 复制原始符号表
    memcpy(new_symtab, (uint8_t*)header+symtab.symoff, old_symtabsize);
    memcpy(new_strtab, (uint8_t*)header+symtab.stroff, old_strsize);
    
    // memory read --force --outfile /Users/xingbi/dumpapp/Snapchat/测试/mem.txt --count 1000 0x6080000fe680
    // memory read -b --force --outfile /Users/xingbi/dumpapp/Snapchat/测试/mem.bin --count new_symtabsize+new_strsize new_tabdata
    // memory read -b --force --outfile /Users/xingbi/dumpapp/Snapchat/测试/mem2.bin --count 25949670 4570103808
    // new_tabdata
    // ---------------------| -------------------| ---------------------| --------
    // new_symtab                                new_strtab
    
    // 写字符串表
    uint8_t* w_symtab = new_symtab + old_symtabsize;
    char* w_strtab = (char*)new_strtab + old_strsize;
    for (auto it : symbolList) {
        uint64_t addr = it.first;
        std::string& symbol = it.second;
        auto cpylen = symbol.length()+1; // 把'\0'也拷贝过去
        memcpy(w_strtab, symbol.c_str(), cpylen);
        
        struct nlist_64 nlist;
        nlist.n_un.n_strx = (uint32_t)((uint8_t*)w_strtab - new_strtab); // 字符串位于字符表里的偏移
        nlist.n_type = addr == 0 ? 1 : N_PEXT | N_SECT;
        nlist.n_sect = sect_for_address((addr & 0xffffffff) + 0x100000000, header);
        nlist.n_desc = 0;
        nlist.n_value = it.first;
        memcpy(w_symtab, &nlist, sizeof(nlist));
        
        w_symtab += symsize;
        w_strtab += cpylen;
    }
    
    new_symbol nsymbol;
    nsymbol.symtab = new_symtab;
    nsymbol.symsize = new_symsize;
    nsymbol.strtab = new_strtab;
    nsymbol.strsize = new_strsize;
    nsymbol.memptr = memptr;
    *result = nsymbol;
    
    return true;
}

bool create_symbol_with_file(const char* filename, const struct mach_header_64* header, new_symbol* result)
{
    symbol_addr_map symbolList;
    if (!read_symbol(filename, &symbolList))
    {
        printf("read symbol error!\n");
        return false;
    }
    
    return create_symbol_with_list(symbolList, header, result);
}

void hacho_test(test_result* result)
{
    const char* filename = "/Library/MobileSubstrate/DynamicLibraries/snapchat_symbol.txt";
    
    symbol_addr_map symbolList;
    result->addr = read_symbol(filename, &symbolList);
//    if (!read_symbol(filename, symbolList))
//    {
//        printf("read symbol error!\n");
//        result->symbol = "read symbol error!";
//        //return;
//    }
    
    
    for (auto it : symbolList) {
        result->addr = it.first;
        //result->symbol = it.second.c_str();
        result->symbol = (char*)malloc(it.second.length()+1);
        memcpy((void*)result->symbol, it.second.c_str(), it.second.length()+1);
        break;
    }
}

struct ProgramVars {
    struct mach_header*    mh;
    int*        NXArgcPtr;
    const char***    NXArgvPtr;
    const char***    environPtr;
    const char**    __prognamePtr;
};





//new_symbol* my_symbol()
//{
//    return &static_result;
//}


static symbol_addr_map* symbolList;

void _rebind_symbols_for_image(const struct mach_header *header,
                               intptr_t slide)
{
    if (header != _dyld_get_image_header(0)) return;
    
    new_symbol nsymbol;
    create_symbol_with_list(*symbolList, (const struct mach_header_64*)header, &nsymbol);

    
    segment_command_64 *cur_seg_cmd;
    segment_command_64 *linkedit_segment = NULL;
    struct symtab_command* symtab_cmd = NULL;
    struct dysymtab_command* dysymtab_cmd = NULL;
    
    uintptr_t cur = (uintptr_t)header + sizeof(mach_header_64);
    for (int i = 0; i < header->ncmds; i++, cur += cur_seg_cmd->cmdsize) {
        cur_seg_cmd = (segment_command_64 *)cur;
        if (cur_seg_cmd->cmd == LC_SEGMENT_64) {
            if (strcmp(cur_seg_cmd->segname, SEG_LINKEDIT) == 0) {
                linkedit_segment = cur_seg_cmd;
            }
        } else if (cur_seg_cmd->cmd == LC_SYMTAB) {
            symtab_cmd = (struct symtab_command*)cur_seg_cmd;
        } else if (cur_seg_cmd->cmd == LC_DYSYMTAB) {
            dysymtab_cmd = (struct dysymtab_command*)cur_seg_cmd;
        }
    }
    
    if (!symtab_cmd || !dysymtab_cmd || !linkedit_segment ||
        !dysymtab_cmd->nindirectsyms) {
        return;
    }
    
    uint8_t* linkedit_base = (uint8_t*)((uintptr_t)slide + linkedit_segment->vmaddr - linkedit_segment->fileoff);
    
    
#define F_LOAD(type, var, offset) type& var = *(type*)((char*)header + (offset & 0xFFFFFFFF));

    uint32_t command_offset = sizeof(struct mach_header_64);

    for (int i=0; i<header->ncmds; i++)
    {
        F_LOAD(struct segment_command_64, command, command_offset);

        if (command.cmd == LC_SYMTAB)
        {
            F_LOAD(struct symtab_command, ucmd, command_offset);
            printf("LC_SYMTAB symoff: 0x%X nsyms: %d stroff: %d strsize: %d\n", ucmd.symoff, ucmd.nsyms, ucmd.stroff, ucmd.strsize);

//            size_t stroff = ucmd.stroff; // 字符串表

            
            
//            *(uint32_t*)(slide + off) = nsymbol.symtab - (uint8_t*)slide; // test符号表
//            size_t symoff = nsymbol.symtab - (uint8_t*)header; // test符号表
//            size_t stroff = nsymbol.strtab - (uint8_t*)header; // test字符串表
            
//            ucmd.symoff = nsymbol.symtab - (uint8_t*)header;
//            ucmd.stroff = nsymbol.strtab - (uint8_t*)header;
//
//            size_t symoff = ucmd.symoff;
//            size_t stroff = ucmd.stroff;
//            uint8_t* base = (uint8_t*)header;
            
            size_t symoff = ucmd.symoff;
            size_t stroff = ucmd.stroff;
            uint8_t* base = (uint8_t*)header;
            
            struct symtab_command* slide_ucmd = (struct symtab_command*)(((uint8_t*)&ucmd - (uint8_t*)header) + linkedit_base);

//            dysymtab_cmd->indirectsymoff = nsymbol.symtab - linkedit_base;
//            dysymtab_cmd->nindirectsyms  = 49000;
            
//            slide_ucmd->symoff = nsymbol.symtab - linkedit_base; // test符号表
//            slide_ucmd->stroff = nsymbol.strtab - linkedit_base; // test符号表
//
//            size_t symoff = slide_ucmd->symoff;
//            size_t stroff = slide_ucmd->stroff;
//            uint8_t* base = linkedit_base;

            // 遍历符号
            for (int i=0; i<ucmd.nsyms; i++) {
                //F_LOAD(struct nlist, nlist, ucmd.symoff + i*16); // sizeof(struct nlist) 12 or 16
                struct nlist& nlist= *(struct nlist*)(base + symoff + i*16);
                const char *ptr = (const char*)(base + stroff + nlist.n_un.n_strx);
                
                uint8_t n_sect = sect_for_address(nlist.n_value + 0x100000000, (const struct mach_header_64*)base);
                uint8_t n_type = nlist.n_value == 0 ? 1 : N_PEXT | N_SECT;
                
                printf("%llx [%d] %d %d type:%d sect:%d desc:%d %x %s\n", ucmd.symoff + i*16, i, n_sect, n_type, nlist.n_type, nlist.n_sect, nlist.n_desc, nlist.n_value, ptr);
                //                printf("[%d] type:%d sect:%d desc:%d value:%x %s\n", i, nlist.n_type, nlist.n_sect, nlist.n_desc, nlist.n_value, ptr);
            }
        }

        command_offset += command.cmdsize;
    }
}




void test()
{
    const char* filename = "/Library/MobileSubstrate/DynamicLibraries/snapchat_symbol.txt";
    
    symbolList = new symbol_addr_map();
    if (!read_symbol(filename, symbolList))
    {
        printf("error\n");
        return;
    }
    
    _dyld_register_func_for_add_image(_rebind_symbols_for_image);
}



__attribute__((constructor))
void dumptofile(int argc, const char **argv, const char **envp, const char **apple, struct ProgramVars *pvars)
{
//    test();
}
