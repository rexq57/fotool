#pragma once
//
//  rebuild_symtab.hpp
//  macho
//
//  Created by Viktor Pih on 2019/6/15.
//  Copyright © 2019 com.wzbbj. All rights reserved.
//

#include <mach-o/dyld.h>
#include <string.h>
#include <map>
#include <string>

#if defined(__cplusplus)
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

struct new_symbol{
    uint8_t* symtab;
    size_t symsize;
    uint8_t* strtab;
    size_t strsize;
    void* memptr;
};

struct test_result {
    size_t addr;
    const char* symbol;
};

typedef void (*pfunc_read)(const char* addr, const char* symbol, void* info);
EXTERN void read_symbol_func(const char* addr, const char* symbol, void* info);

EXTERN void* load_all(const char* filename);


typedef void (*pfunc_read_symtab)(const struct nlist* nlist, const char* symptr, void* info);
void read_symtab(const struct mach_header_64* header, pfunc_read_symtab func, void* info);


typedef std::map<size_t, std::string> symbol_addr_map;

EXTERN uint64_t entryoff(const struct mach_header_64* header);
EXTERN const struct symtab_command* get_symtab(const struct mach_header_64* header);

EXTERN uint8_t sect_for_address(uint64_t address, const struct mach_header_64* header);

EXTERN int read_symbol(const char* filename, void* info, pfunc_read func=read_symbol_func);
EXTERN bool create_symbol_with_list(const symbol_addr_map& symbolList, const struct mach_header_64* header, new_symbol* result);

EXTERN bool create_symbol_with_file(const char* filename, const struct mach_header_64* header, new_symbol* result);

EXTERN void hacho_test(test_result* result);

//EXTERN new_symbol* my_symbol();
