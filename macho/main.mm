//
//  main.m
//  macho
//
//  Created by Viktor Pih on 2019/6/9.
//  Copyright © 2019 com.wzbbj. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <mach-o/getsect.h>
#include <mach-o/dyld.h>
#include <mach-o/nlist.h>
#include <string>
#include <vector>
#include <map>
#include <functional>
#include <string.h>

#include "rebuild_symtab.h"
#include <dlfcn.h>

void* readAllFile(const char * path, int *length)
{
    FILE * pfile;
    void* data;
 
    pfile = fopen(path, "rb");
    if (pfile == NULL)
    {
        return NULL;
    }
    fseek(pfile, 0, SEEK_END);
    *length = ftell(pfile);
    data = (char *)malloc((*length + 1) * sizeof(char));
    rewind(pfile);
    *length = fread(data, 1, *length, pfile);
    //data[*length] = '\0';
    fclose(pfile);
    return data;
}

void test2()
{
    // AWESetting
    int length;
    {
        //const char* filename = "/usr/lib/system/libsystem_c.dylib";
        const char* filename = "/Users/xingbi/Documents/逆向工程/抖音/9.6.1/Payload/Aweme.app/Aweme";
        void* data = readAllFile(filename, &length);
//        void* handle = dlopen("/usr/lib/system/libsystem_c.dylib", 2);
        const struct mach_header_64* header = (const struct mach_header_64*)data;
        uint32_t sizeofcmds = header->sizeofcmds;
        unsigned long size = 0;
        const struct segment_command_64 *psegment = (const struct segment_command_64*)getsegmentdata((const struct mach_header_64*)data,  "__TEXT", &size);
        printf("\n");
        (void)0;
    }
    
    {
        const char* filename = "/Users/xingbi/Documents/逆向工程/抖音/9.6.1/Payload/Aweme.app/Frameworks/AwemeDylib.framework/AwemeDylib";
        void* data = readAllFile(filename, &length);
        unsigned long size = 0;
        uint8_t *pdata = getsectiondata((const struct mach_header_64*)data,  "__DATA", "__HTSLifeCycle", &size);
        free(data);
    }
}

uint64_t mod_init_addr(struct mach_header_64 *header) {
    const struct section_64 *sec = getsectbynamefromheader_64(header, "__DATA", "__mod_init_func");
    if (sec) {
        return sec->addr;
    }
    return 0;
}

// 需要调用free()
//void* load_all(const char* filename)
//{
//    FILE *fp = fopen(filename, "r");
//    if (!fp) return 0;
//    
//    fseek(fp, 0, SEEK_END);
//    long filesize = ftell(fp);
//    void* buf = malloc(filesize);
//    
//    fseek(fp, 0, SEEK_SET);
//    fread(buf, filesize, 1, fp);
//    
//    fclose(fp);
//    
//    return buf;
//}

uint64_t entryoff()
{
#define LC_REQ_DYLD 0x80000000
#define LC_MAIN (0x28|LC_REQ_DYLD) /* replacement for LC_UNIXTHREAD */
    struct mach_header_64 {
        uint32_t    magic;        /* mach magic number identifier */
        cpu_type_t    cputype;    /* cpu specifier */
        cpu_subtype_t    cpusubtype;    /* machine specifier */
        uint32_t    filetype;    /* type of file */
        uint32_t    ncmds;        /* number of load commands */
        uint32_t    sizeofcmds;    /* the size of all the load commands */
        uint32_t    flags;        /* flags */
        uint32_t    reserved;    /* reserved */
    };
    struct entry_point_command {
        uint32_t  cmd;    /* LC_MAIN only used in MH_EXECUTE filetypes */
        uint32_t  cmdsize;    /* 24 */
        uint64_t  entryoff;    /* file (__TEXT) offset of main() */
        uint64_t  stacksize;/* if not zero, initial stack size */
    };
    const struct mach_header_64* header = (struct mach_header_64*)_dyld_get_image_header(0);
    uint8_t* segment_data_start = (uint8_t*)header + sizeof(struct mach_header_64);
    uint8_t* command_offset = segment_data_start;
    struct segment_command_64* last_command;
    uint64_t  entryoff;
    printf("sizeof(mach_header_64): %d ncmds_offset: %d %ld\n", sizeof(struct mach_header_64), (uint8_t*)&header->ncmds-(uint8_t*)header, (uint32_t)LC_MAIN);
    uint32_t lc_main = LC_MAIN;
    for (int i=0; i<header->ncmds; i++)
    {
        struct segment_command_64* command = (struct segment_command_64*)command_offset;
        
        if(command->cmd == LC_MAIN)
        {
            struct entry_point_command* ucmd = (struct entry_point_command*)command_offset;
            entryoff = ucmd->entryoff;
            break;
        }
        
        command_offset += command->cmdsize;
        last_command = command;
    }
    return (uint64_t)entryoff;
}

////////////////////////////////////////////////////////////////////



void restore_symbol(const symbol_addr_map& symbolList_, const struct mach_header_64* header)
{
    new_symbol nsymbol;
    
    create_symbol_with_list(symbolList_, header, &nsymbol);
    
    uint32_t command_offset = sizeof(struct mach_header_64);
    struct segment_command_64* last_command;
    uint64_t  entryoff;
//    printf("sizeof(mach_header_64): %d ncmds_offset: %d %ld\n", sizeof(struct mach_header_64), (uint8_t*)&header->ncmds-(uint8_t*)header, (uint32_t)LC_MAIN);
    uint32_t lc_main = LC_MAIN;
    
    #define F_LOAD(type, var, offset) type& var = *(type*)((char*)header + (offset & 0xFFFFFFFF));
    
    for (int i=0; i<header->ncmds; i++)
    {
        F_LOAD(struct segment_command_64, command, command_offset);
        
//        printf("%d %d %d\n", command_offset, command_offset2-(uint8_t*)header, command.cmdsize);
        
        if(command.cmd == LC_MAIN)
        {
            F_LOAD(struct entry_point_command, ucmd, command_offset);
            entryoff = ucmd.entryoff;
            break;
        }
        else if (command.cmd == LC_SYMTAB)
        {
            F_LOAD(struct symtab_command, ucmd, command_offset);
            printf("LC_SYMTAB symoff: 0x%X nsyms: %d stroff: %d strsize: %d\n", ucmd.symoff, ucmd.nsyms, ucmd.stroff, ucmd.strsize);
            
//            size_t strtab = ucmd.stroff; // 字符串表
            
            ucmd.symoff = nsymbol.symtab - (uint8_t*)header; // test符号表
            size_t strtab = nsymbol.strtab - (uint8_t*)header; // test字符串表
            
            // 遍历符号
            for (int i=0; i<ucmd.nsyms; i++) {
                //F_LOAD(struct nlist, nlist, ucmd.symoff + i*16); // sizeof(struct nlist) 12 or 16
                struct nlist& nlist= *(struct nlist*)((uint8_t*)header + ucmd.symoff + i*16);
                const char *ptr = (const char*)((uint8_t*)header + strtab + nlist.n_un.n_strx);
                
                uint8_t n_sect = sect_for_address(nlist.n_value + 0x100000000, header);
                uint8_t n_type = nlist.n_value == 0 ? 1 : N_PEXT | N_SECT;
                
                printf("%llx [%d] %d %d type:%d sect:%d desc:%d %x %s\n", ucmd.symoff + i*16, i, n_sect, n_type, nlist.n_type, nlist.n_sect, nlist.n_desc, nlist.n_value, ptr);
//                printf("[%d] type:%d sect:%d desc:%d value:%x %s\n", i, nlist.n_type, nlist.n_sect, nlist.n_desc, nlist.n_value, ptr);
            }
        }
        
        command_offset += command.cmdsize;
//        last_command = command;
    }
    
    (void)free(nsymbol.memptr); // 会崩溃
}

#include "macho.h"

void test_bl()
{
    // dc 00 00 94
    // BL    #0x370
    uint32_t cmd = 0x940000dc;
    uint32_t imm26 = cmd ^ 0x94000000;
    assert(cmd & 0x94000000);
    uint32_t offset = imm26 * 4;
    
    printf("bl #0x%x\n", offset);
    exit(0);
}

typedef enum : NSUInteger {
    ExtendType_UXTB = 0, // 000
    ExtendType_UXTH, // 001
    ExtendType_UXTW, // 010
    ExtendType_UXTX, // 011
    ExtendType_SXTB,
    ExtendType_SXTH,
    ExtendType_SXTW,
    ExtendType_SXTX
} ExtendType;

ExtendType DecodeRegExtend(uint8_t op) {
    return (ExtendType)op;
}

#define FALSE 0
#define TRUE 1

uint32_t ExtendReg(uint32_t reg, ExtendType type, uint32_t shift) {
    assert(shift >= 0 && shift <= 4);
    uint32_t val = reg;//X[reg];
    int _unsigned;
    uint32_t len;
    switch (type) {
        case ExtendType_SXTB: _unsigned = FALSE; len = 8;break;
        case ExtendType_SXTH: _unsigned = FALSE; len = 16;break;
        case ExtendType_SXTW: _unsigned = FALSE; len = 32;break;
        case ExtendType_SXTX: _unsigned = FALSE; len = 64;break;
        case ExtendType_UXTB: _unsigned = TRUE; len = 8;break;
        case ExtendType_UXTH: _unsigned = TRUE; len = 16;break;
        case ExtendType_UXTW: _unsigned = TRUE; len = 32;break;
        case ExtendType_UXTX: _unsigned = TRUE; len = 64;break;
        default:
            break;
    }
    
    // Note the extended width of the intermediate value and
    // that sign extension occurs from bit <len+shift-1>, not
    // from bit <len-1>. This is equivalent to the instruction
    // [SU]BFIZ Rtmp, Rreg, #shift, #len
    // It may also be seen as a sign/zero extend followed by a shift:
    // LSL(Extend(val<len-1:0>, N, unsigned), shift);
//    len = Min(len, N - shift);
//    return Extend(val<len-1:0> : Zeros(shift), N, unsigned);
    return 0;
}

// 来自arm官方文档 https://static.docs.arm.com/ddi0596/a/DDI_0596_ARM_a64_instruction_set_architecture.pdf

void test_ldr()
{
    // 90 2f 03 58
    // LDR    X16, #0x65F0

    // ldr 寄存器
//    uint32_t cmd = 0x58032f90;
//    assert(cmd & 0xB8600800);
//    uint32_t size = (cmd & 0xC0000000) >> 30;
//    uint32_t option = (cmd & 0xE000) >> 13;
//    uint32_t S = (cmd & 0x1000) >> 12; // 0
//    uint32_t m = (cmd & 0x1F0000) >> 16; // 3
//    uint32_t n = (cmd & 0x3E0) >> 5; // 1c
//    uint32_t t = cmd & 0x1F; // 寄存器 16
//    uint32_t scale = size;
//
//    ExtendType extend_type = DecodeRegExtend(option);
//    uint32_t shift = S == 1 ? scale : 0;
//
////    uint32_t offset = ExtendReg(m, extend_type, shift);
//
//    uint32_t offset = (m << 16) / 8 + (n << 5);
//
//    printf("ldr x%d #0x%x\n", t, offset);
    
    // ldr 立即数
//    uint32_t cmd = 0x58032f90;
//    assert(cmd & 0xB8400C00);
//
//    uint32_t size = (cmd & 0xC0000000) >> 30;
//    uint32_t imm9 = (cmd & 0x1FF000) >> 12;
//    uint32_t imm12 = (cmd & 0x3FFC00) >> 10;
//    uint32_t n = (cmd & 0x3E0) >> 5; // 1c
//    uint32_t t = cmd & 0x1F; // 寄存器 16
//    uint32_t scale = size;
//    uint32_t datasize = 8 << scale;
//
//    uint32_t offset = (imm12 << 10) / 8;
//
//    printf("ldr x%d #0x%x 0x%x\n", t, offset, datasize);
    
    // ldr x[n], #offset
    // b0 31 03 58
    // LDR    X16, #0x6634
    
    uint32_t cmd = 0x580331b0;
    assert(cmd & 0x18000000);
    
    uint32_t opc = (cmd & 0x40000000) >> 30;
    uint32_t imm19 = (cmd & 0xFFFFE0) >> 5; // 1c
    uint32_t t = cmd & 0x1F; // 寄存器 16
    
    uint32_t offset = (imm19 << 5) / 8;
    
    printf("ldr x%d #0x%x\n", t, offset);
    
    exit(0);
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
//        NSLog(@"Hello, World!");
        
//        test_ldr();
        
        void* data = load_all("/Users/xingbi/Library/Developer/Xcode/DerivedData/metalhook-dtlgyeaquznaahfbsacevccfarvk/Build/Products/Debug-iphoneos/metalhook.dylib");
        const struct mach_header* header = (const struct mach_header*)data; // 来自内存
        
        std::map<uintptr_t, const char*> dysym_vec;
        
        macho macho;
        macho.setup(header, 0);
        
        macho.read_symtab([](nlist_t* nlist, const char* symptr, void* info){
            if (nlist->n_type == 1) // 1是extern
            printf("sym %d %p %s\n", nlist->n_type, nlist->n_value, symptr);
        }, (void*)header);
        
        macho.read_dysymtab([](uintptr_t offset, const char* symptr, void* info){
//            if (nlist->n_type == 1) // 1是extern
//            printf("%p %s\n", offset, symptr);
            (*(std::map<uintptr_t, const char*>*)info)[offset] = symptr;
        }, (void*)&dysym_vec);
        
        for (auto it : dysym_vec) {
            printf("%p %s\n", it.first, it.second);
        }

//        const struct mach_header_64* header = (const struct mach_header_64*)_dyld_get_image_header(0); // 来自app
//
//        const char* filename = "/Users/xingbi/dumpapp/Snapchat/测试/snapchat_symbol.txt";
        
//        symbol_addr_map symbolList;
//        if (!read_symbol(filename, &symbolList))
//        {
//            printf("error\n");
//            return 0;
//        }
//
//        restore_symbol(symbolList, header);
        
//        test_result result;
//        hacho_test(filename, header, &result);
        
//        new_symbol nsymbol;
//        create_symbol_with_file("/Users/xingbi/dumpapp/Snapchat/测试/snapchat_symbol.txt", header, &nsymbol);
//        (void)free(nsymbol.memptr); // 会崩溃
        
//        printf("%llu %d\n", entryoff(), N_PEXT | N_SECT);
        
//        free(data);
        
//        test2();
        
    }
    return 0;
}
