//
//  macho.cpp
//  macho
//
//  Created by Viktor Pih on 2020/5/12.
//  Copyright © 2020 com.wzbbj. All rights reserved.
//

#include "macho.h"
#include <dlfcn.h>
#include <sys/types.h>
#include <string.h>



macho::macho() {

}

section_t* get_section(const struct mach_header *header, uint32_t cmd, const char* seg_name, int section_type) {
    segment_command_t *cur_seg_cmd;
    uintptr_t cur = (uintptr_t)header + sizeof(mach_header_t);
    for (uint i = 0; i < header->ncmds; i++, cur += cur_seg_cmd->cmdsize) {
        cur_seg_cmd = (segment_command_t *)cur;
        if (cur_seg_cmd->cmd == cmd) {
            if (strcmp(cur_seg_cmd->segname, seg_name) != 0) {
                continue;
            }
            for (uint j = 0; j < cur_seg_cmd->nsects; j++) {
                section_t *sect = (section_t *)(cur + sizeof(segment_command_t)) + j;
                if ((sect->flags & SECTION_TYPE) == section_type) {
//                    printf("slide:%p sect->addr:%p\n", slide, sect->addr);
//                    for (uint i = 0; i < sect->size / sizeof(void *); i++) {
//
//                    }
                    return sect;
                }
            }
        }
    }
    return NULL;
}

void macho::setup(const struct mach_header *header, intptr_t slide)
{
//    const struct mach_header *header = m_header;
    
    // 这是支持直接从内存来的header，而不是必须来自加载的模块
//    Dl_info info;
//    if (dladdr(header, &info) == 0) {
//      return;
//    }
    if (!header) return;
    
    segment_command_t *cur_seg_cmd;
    segment_command_t *linkedit_segment = NULL;
    struct symtab_command* symtab_cmd = NULL;
    struct dysymtab_command* dysymtab_cmd = NULL;

    uintptr_t cur = (uintptr_t)header + sizeof(mach_header_t);
    for (uint i = 0; i < header->ncmds; i++, cur += cur_seg_cmd->cmdsize) {
        cur_seg_cmd = (segment_command_t *)cur;
        if (cur_seg_cmd->cmd == LC_SEGMENT_ARCH_DEPENDENT) {
            if (strcmp(cur_seg_cmd->segname, SEG_LINKEDIT) == 0) {
                linkedit_segment = cur_seg_cmd;
            }
        } else if (cur_seg_cmd->cmd == LC_SYMTAB) {
            symtab_cmd = (struct symtab_command*)cur_seg_cmd;
        } else if (cur_seg_cmd->cmd == LC_DYSYMTAB) {
            dysymtab_cmd = (struct dysymtab_command*)cur_seg_cmd;
        }
    }
    
    // 检查符号表等是否有效
    if (!symtab_cmd || !dysymtab_cmd || !linkedit_segment ||
        !dysymtab_cmd->nindirectsyms) {
      return;
    }
    
    // Find base symbol/string table addresses
    uintptr_t linkedit_base = (uintptr_t)slide + linkedit_segment->vmaddr - linkedit_segment->fileoff;
    if (linkedit_base == 0) {
        linkedit_base = (uintptr_t)header;
        slide = (uintptr_t)header;
    }
    
    m_strtab = (char *)(linkedit_base + symtab_cmd->stroff); // 字符串表
    m_indirect_symtab = (uint32_t *)(linkedit_base + dysymtab_cmd->indirectsymoff);
    
    m_symtab_cmd = symtab_cmd;
    m_dysymtab_cmd = dysymtab_cmd;
    m_linkedit_segment = linkedit_segment;
    m_linkedit_base = linkedit_base;
    m_header = header;
    m_slide = slide;
}

void macho::read_symtab(pfunc_read_symtab func, void* user_info)
{
    const struct mach_header *header = m_header;
    uintptr_t linkedit_base = m_linkedit_base;
    struct symtab_command* symtab_cmd = m_symtab_cmd;
    char *strtab = m_strtab;
    
//    Dl_info info;
//    if (dladdr(header, &info) == 0) {
//      return;
//    }
    if (!header) return;
    
    // 遍历符号表
    for (int i=0; i<symtab_cmd->nsyms; i++) {

        nlist_t* nlist= (nlist_t*)((uint8_t*)header + symtab_cmd->symoff + i*16);
        const char *ptr = strtab + nlist->n_un.n_strx;
        
        func(nlist, ptr, user_info);
    }
}

//无符号整型32位
uint32_t bswap_32(uint32_t x)
{
    return (((uint32_t)(x) & 0xff000000) >> 24) | \
           (((uint32_t)(x) & 0x00ff0000) >> 8) | \
           (((uint32_t)(x) & 0x0000ff00) << 8) | \
           (((uint32_t)(x) & 0x000000ff) << 24) ;
}

void macho::read_dysymtab(pfunc_read_dysymtab func, void* user_info)
{
    const struct mach_header *header = m_header;
    uintptr_t linkedit_base = m_linkedit_base;
    struct symtab_command* symtab_cmd = m_symtab_cmd;
    struct dysymtab_command* dysymtab_cmd = m_dysymtab_cmd;
    
    char *strtab = m_strtab;
    uint32_t *indirect_symtab = m_indirect_symtab;
    
    uintptr_t slide = m_slide;
    
    if (!header) return;
    
//    for (int i=0; i<dysymtab_cmd->nindirectsyms; i++) {
//
////        nlist_t* nlist= (nlist_t*)((uint8_t*)header + symtab_cmd->symoff + i*16);
////        const char *ptr = strtab + nlist->n_un.n_strx;
//
//        func(0, 0, user_info);
//    }
    
    nlist_t *symtab = (nlist_t *)(linkedit_base + symtab_cmd->symoff);
    
//    printf("fuck %d\n", dysymtab_cmd->nindirectsyms);
//    for (int i=0; i<dysymtab_cmd->nindirectsyms; i++) {
//        // uintptr_t addr = *(uintptr_t*)((uintptr_t)slide + indirect_symtab[i]);
////        struct nlist& nlist= *(struct nlist*)((uint8_t*)header + symtab_cmd.symoff + i*16);
////        struct nlist& nlist= *(struct nlist*)((uint8_t*)header + symtab_cmd.symoff + i*16);
//        printf("%p\n", indirect_symtab[i]);
//    }
    
    
    if (0)
    {
        // __stubs
        section_t *sect = get_section(header, LC_SEGMENT_ARCH_DEPENDENT, SEG_TEXT, S_SYMBOL_STUBS);
        
        printf("slide:%p sect->addr:%p %d\n", slide, sect->addr, sect->size / sizeof(void *));
        for (uint i = 0; i < sect->size / sizeof(void *); i++) {
            uint32_t offset = sect->addr + i*0xC;
            uint32_t cmd = *(uint32_t*)(slide + offset + 0x4);
            printf("%p %x\n", offset, bswap_32(cmd));
        }
    }
    
    if (0)
    {
        // __la_symbol_ptr
        section_t *sect = get_section(header, LC_SEGMENT_ARCH_DEPENDENT, SEG_DATA, S_LAZY_SYMBOL_POINTERS);
        
        printf("slide:%p sect->addr:%p %d\n", slide, sect->addr, sect->size / sizeof(void *));
        for (uint i = 0; i < sect->size / sizeof(void *); i++) {
            uint32_t offset = sect->addr + i*0x8; // 指针地址
//            uint32_t cmd = *(uint32_t*)(slide + offset + 0x4);
            
            printf("%p\n", offset);
        }
    }
    
    if (1)
    {
        segment_command_t* cur_seg_cmd;
        uintptr_t cur = (uintptr_t)header + sizeof(mach_header_t);
        
        for (uint i = 0; i < header->ncmds; i++, cur += cur_seg_cmd->cmdsize) {
            cur_seg_cmd = (segment_command_t *)cur;
            if (cur_seg_cmd->cmd == LC_SEGMENT_ARCH_DEPENDENT) {
                if (strcmp(cur_seg_cmd->segname, SEG_DATA) != 0 &&
                    strcmp(cur_seg_cmd->segname, SEG_DATA_CONST) != 0) {
                    continue;
                }
                for (uint j = 0; j < cur_seg_cmd->nsects; j++) {
                    section_t *sect = (section_t *)(cur + sizeof(segment_command_t)) + j;
                    if ((sect->flags & SECTION_TYPE) == S_LAZY_SYMBOL_POINTERS
                        /*|| (sect->flags & SECTION_TYPE) == S_NON_LAZY_SYMBOL_POINTERS*/ ) {
                        
    //                    perform_rebinding_with_section(rebindings, sect, slide, symtab, strtab, indirect_symtab);
                        
                        // sect->addr 就是指针地址，每个8字节
                        printf("slide:%p sect->addr:%p %d\n", slide, sect->addr, sect->size / sizeof(void *));
                        
                        // 遍历section
                        uint32_t *indirect_symbol_indices = indirect_symtab + sect->reserved1;
                        void **indirect_symbol_bindings = (void **)((uintptr_t)slide + sect->addr);
                        for (uint i = 0; i < sect->size / sizeof(void *); i++) {
                            
                            uintptr_t la_symbol_ptr = sect->addr + i*8;
                            uint32_t symtab_index = indirect_symbol_indices[i];
                            if (symtab_index == INDIRECT_SYMBOL_ABS || symtab_index == INDIRECT_SYMBOL_LOCAL ||
                                symtab_index == (INDIRECT_SYMBOL_LOCAL   | INDIRECT_SYMBOL_ABS)) {
                              
                                continue;
                            }
                            nlist_t* nlist = &symtab[symtab_index];
                            uint32_t strtab_offset = nlist->n_un.n_strx; // symtab[symtab_index].n_un.n_strx;
                            char *symbol_name = strtab + strtab_offset;
                            bool symbol_name_longer_than_1 = symbol_name[0] && symbol_name[1];
                            // struct rebindings_entry *cur = rebindings;
                            
                            // 如果 indirect_symbol_bindings[i] 没有地址，说明不是间接跳转，不存在stubs中
                            // if (indirect_symbol_bindings[i])
                            {
                                // printf("%p %d 0x%x ", la_symbol_ptr, symtab_index, nlist->n_value);
                                func(la_symbol_ptr, symbol_name, user_info);
                            }
                        }
                    }
                }
            }
        }
    }
}
