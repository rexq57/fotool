//
//  macho.hpp
//  macho
//
//  Created by Viktor Pih on 2020/5/12.
//  Copyright © 2020 com.wzbbj. All rights reserved.
//

#ifndef macho_hpp
#define macho_hpp

#include <stdio.h>
#include <mach-o/getsect.h>
#include <mach-o/dyld.h>
#include <mach-o/nlist.h>

#ifdef __LP64__
typedef struct mach_header_64 mach_header_t;
typedef struct segment_command_64 segment_command_t;
typedef struct section_64 section_t;
typedef struct nlist_64 nlist_t;
#define LC_SEGMENT_ARCH_DEPENDENT LC_SEGMENT_64
#else
typedef struct mach_header mach_header_t;
typedef struct segment_command segment_command_t;
typedef struct section section_t;
typedef struct nlist nlist_t;
#define LC_SEGMENT_ARCH_DEPENDENT LC_SEGMENT
#endif

#ifndef SEG_DATA_CONST
#define SEG_DATA_CONST  "__DATA_CONST"
#endif

class macho {
    
    const struct mach_header *m_header = NULL;
    uintptr_t m_slide = NULL;
    
    uintptr_t m_linkedit_base = NULL;
    segment_command_t *m_linkedit_segment = NULL;
    
    struct symtab_command* m_symtab_cmd = NULL;
    struct dysymtab_command* m_dysymtab_cmd = NULL;
    
    char* m_strtab = NULL;
    uint32_t* m_indirect_symtab = NULL;

public:
    macho();
    
    void setup(const struct mach_header *header, intptr_t slide);
    
    typedef void (*pfunc_read_symtab)(nlist_t* nlist, const char* symptr, void* info);
    void read_symtab(pfunc_read_symtab func, void* user_info);
    
    typedef void (*pfunc_read_dysymtab)(uintptr_t offset, const char* symptr, void* info);
    void read_dysymtab(pfunc_read_dysymtab func, void* user_info);
};

#endif /* macho_hpp */
